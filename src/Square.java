import java.util.HashMap;

public class Square {
	int index;    // represent the index of the square
	String type; // type could be S = start, G = goal, F = forbidden, W = wall, " " = normal
	double reward; // goals = +100, forbidden = -100, Normal = 0, Wall = -0.1
	HashMap<Main.Direction, Double> qValues;
	Main.Direction[] actions; 
	
	public Square(int index, String t) {
		this.index = index;
		type = t;
		// init actions
		actions = new Main.Direction[4];
		actions[0] = Main.Direction.UP;actions[1] = Main.Direction.DOWN;
		actions[2] = Main.Direction.LEFT;actions[3] = Main.Direction.RIGHT;
		if (t.equals("G")) {
			reward = 100.0;
			// init actions for goal state
			actions = new Main.Direction[1];
			actions[0] = Main.Direction.EXIT;
		}else if (t.equals("F")) {
			reward = -100.0;
			actions = new Main.Direction[1];
			actions[0] = Main.Direction.EXIT;
		}else if (t.equals("W")){
			reward = -0.1; //-1.0
		}else {
			reward = 0.0;
		}
		
		// init q-value for each state each action
		qValues = new HashMap<> ();
		for (int j = 0; j < actions.length; j++) {
			qValues.put(actions[j], 0.0);
		}

//      test fot map
//		System.out.print("squar index: " + index + "[");
//		for (HashMap.Entry<Main.Direction, Integer> entry : qValues.entrySet()) {
//			System.out.println(entry.getKey() + ": " + entry.getValue());
//		}
//		System.out.println("]");
		
	}
	
	public Main.Direction findMax() {
		Main.Direction index = null;
		double max = -100000.0;
		for (int i = 0; i < actions.length; i++) {
			if (qValues.get(actions[i]) > max) {
				max = qValues.get(actions[i]);
				index = actions[i];
			}
		}
		return index;
	}
	
	public String toString() {
		return "[" + String.valueOf(index) + ", " + type +  "]";
		//return "[" + String.valueOf(index) + ", " + type +  ", " + reward + "]";
	}
}
