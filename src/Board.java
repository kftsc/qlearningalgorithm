import java.util.Random;


public class Board {
	final double epsilon = 0.1;
	final double gamma = 0.2;
	final double alpha = 0.1;
	final double r = -0.1;
	String debugS = "";
	Square[][] board;
	//Agent agent;
	int x, y; //represent curent angent position
	int new_x, new_y; // the new position
	int s_x, s_y; // the init_x and init_y for agent
	
	public Board(String[] input) {
		// build a 4 * 4 board
		board = new Square[4][4];
		int count = 1;
		for (int i = 3; i >= 0; i--) {
			for (int j = 0; j < 4; j ++) {
				if (count == 2) {
					board[i][j] = new Square (count, "S");
					// init agent position
					x = i;
					y = j;
					s_x = x;
					s_y = y;
					//agent = new Agent(i,j);
				}
				else if (count == Integer.valueOf(input[0]) || count == Integer.valueOf(input[1])) {board[i][j] = new Square (count, "G");}
				else if (count == Integer.valueOf(input[2])) {board[i][j] = new Square (count, "F");}
				else if (count == Integer.valueOf(input[3])) {board[i][j] = new Square (count, "W");}
				else {board[i][j] = new Square (count, " ");}
				count++;
			}
		}
	}
	
	
	// train the agent
	// input how many times want to train the agent
	public void train (int times) {
		for (int i = 0; i < times; i ++) {
			//System.out.println("here is " + i + " training!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			while (update());
			resetAgent();
		}
	}
	
	// print policy
	public void printPolicy() {
		Square curS = board[x][y];
		Main.Direction a = curS.findMax();
		int noPath = 0;
		while (a != Main.Direction.EXIT) {
			//System.out.println(curS.index + ", " + a);
			if (!outBound(a)) {
				hitWall();
				//System.out.println("" + curS.index + ": " + a);
				String arrow = "";
				switch(a) {
					case UP:
						arrow = "\u2191".toUpperCase();
						break;
					case DOWN:
						arrow = "\u2193".toUpperCase();
						break;
					case LEFT:
						arrow = "\u2190".toUpperCase();
						break;
					case RIGHT:
						arrow = "\u2192".toUpperCase();
						break;
				}
				System.out.println("" + curS.index + ": " + arrow);
				move();
				curS = board[x][y];
				a = curS.findMax();
				
				noPath++;
				if (noPath > 100) {
					System.out.println("cannot find path");
					return;
				}
			}
		}
//		do {
//			curS = board[x][y];
//			a = curS.findMax();
//			if (!outBound(a)) {
//				hitWall();
//				System.out.println("" + curS.index + ": " + a);
//				move();
//			}
//		}while(a != Main.Direction.EXIT);
	}
	// update return true if not exit
	public boolean update() {
		Main.Direction d = epsilonRandom();
		outBound(d);
		debugS += "" + board[x][y].index + " take action : " + d + " - > [";
		qValue(d);
		hitWall();
		move();
		debugS += "agent curent postion (index): " + board[x][y].index;
		//System.out.println(debugS);
		if (d == Main.Direction.EXIT) {
			return false;
		}
		return true;
	}
	
	// update q-value for the taken action in curren state
	public void qValue(Main.Direction a) {
		Square curS = board[x][y];
		Square nextS;
//		if (curS.index == 2 && a == Main.Direction.DOWN) {
//			System.out.println("x: " + x + ", y: " + y + " new_x: " + new_x + ", new_y: " + new_y);
//		}
		
		if (curS.actions.length == 1 || (new_x == x && new_y == y)) {
			nextS = null;
		}else {
			nextS = board[new_x][new_y];
		}
		
//		if (new_x == x && new_y == y) {
//			System.out.println("here");
//			nextS = null;
//		}else {
//			nextS = board[new_x][new_y];
//		}
		double reward = r + curS.reward;
		double maxV;
		if (nextS != null) {
			maxV = nextS.qValues.get(nextS.findMax());
		}else {
			maxV = 0.0;
		}
		
		double qv = curS.qValues.get(a);
		double new_qv = (1.0 - alpha) * qv + alpha * (reward + gamma * maxV);
		new_qv = Math.round(new_qv * 100000) / 100000.0;
		//double overflow = Math.round(new_qv * 10000) / 10000.0;
		//System.out.println(overflow);
		//System.out.print("new qvalue for (" + a + ") =" + new_qv + "] -> ");
		//debugS = "";
		debugS += "new qvalue for (" + a + ") =" + "(1 - " + alpha + ") * " + qv + " + " + alpha + " * (" + reward + " + " + gamma + " * " + maxV + ") = " + new_qv + "] -> ";

		// update q-value for the taken action in current state
		curS.qValues.replace(a, new_qv);
//		if (a == Main.Direction.RIGHT && curS.index == 2) {
//			System.out.println(debugS);
//		}
	}
	// epsilon random function
	// return an action for current state based on random (possibility epsilon), based on correct policy (possibility 1 - epsilon);
	public Main.Direction epsilonRandom() {
		// the current state is board[x][y]
		Square curS = board[x][y];
		Random rand = new Random(); 
		int p = rand.nextInt(10);
		if (p == 0) {
			// pick action randomly
			
			// the length of actions array
			int l = curS.actions.length;
			//System.out.print("randomly: ");
			debugS = "randomly: ";
			return curS.actions[rand.nextInt(l)];
		}else {
			// pick action based correct policy
			// find the action with the largest q-value
			//System.out.print("correctly: ");
			debugS = "correctlly: ";
			return curS.findMax();
		}
	}
	
	// to move
	public void move () {
		x = new_x;
		y = new_y;
	}
	
	// hit the wall
	public void hitWall() {
		if (board[new_x][new_y].type.equals("W")) {
			// cannot move to wall
			//debugS = "";
			//System.out.println("hit wall!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			new_x = x;
			new_y = y;
		}
	}
	// out of bound
	public boolean outBound(Main.Direction d) {
		int dx = 0; 
		int dy = 0;
		switch (d) {
			case UP:
				dx = -1;
				break;
			case DOWN:
				dx = 1;
				break;
			case LEFT:
				dy = -1;
				break;
			case RIGHT:
				dy = 1;
				break;
		}
		new_x = x + dx;
		new_y = y + dy;
		
		// out of bound
		if (new_x < 0 || new_x > 3 ||
				new_y < 0 ||new_y > 3) {
			new_x = x;
			new_y = y;
		}
		return new_x < 0 || new_x > 3 ||
				new_y < 0 ||new_y > 3;
	}
	
	// reset agent to start state
	public void resetAgent() {
		x = s_x;
		y = s_y;
	}
	
	// find the square in board by index
	public Square findSquare(int index) {
		for(int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (board[i][j].index == index) {
					return board[i][j];
				}
			}
		}
		return null;
	}
	// print the board
	public String toString() {
		String r = "";
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				r += board[i][j].toString();
			}
			r += "\n";
		}
		//System.out.println(r);
		//System.out.println();
		return r;
	}
}
