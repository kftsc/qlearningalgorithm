import java.util.HashMap;
import java.util.Scanner;

public class Main {
	public enum Direction {UP, DOWN, LEFT, RIGHT, EXIT};  // direction
	public static void main (String[] args) {
		//System.out.println("hello world!!!!!!!!!!!!!!!!!");
		System.out.print("your inputs: ");
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		sc.close();
		
		System.out.println("input is : " + input);
		String[] inputs = null;
		Board b = null;
		String t = null;
		String extra = null;
		try {
			inputs = input.split(" ", -1);
			t = inputs[4];
			if (t.equals("q")) {
				extra = inputs[5];
			}
			b = new Board(inputs);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		b.train(10000);
		System.out.println(b);
		if (t.equals("p")) {
			System.out.println("print");
			b.printPolicy();
		}else if (t.equals("q")) {
			Square s = b.findSquare(Integer.valueOf(extra));
			for (HashMap.Entry<Direction, Double> entry : s.qValues.entrySet()) {
				String arrow = "";
				switch(entry.getKey()) {
					case UP:
						arrow = "\u2191".toUpperCase();
						break;
					case DOWN:
						arrow = "\u2193".toUpperCase();
						break;
					case LEFT:
						arrow = "\u2190".toUpperCase();
						break;
					case RIGHT:
						arrow = "\u2192".toUpperCase();
						break;
				}
				System.out.println(arrow + ": " + entry.getValue());
			}
		}
	

		
		
	}
}
