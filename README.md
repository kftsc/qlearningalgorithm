## Description:
    In a 4*4 board, each of the 16 squares has a unique index.
    There are five special squares on the board. These five squares have the type of start(S), goal(G),
    forbidden(F), and wall(W) squares. The remaining 11 squares are empty ordinary squares. The starting
    square is fixed and always at square 2. The location of the two goals,
    forbidden, and wall squares are determined from the input. An agent has four possible actions
    of going to the north, east, south and west. The board is bounded from the sides.

## Input:
    The input to the program consists of four numbers, one character, and possibly an additional
    number [# # # X (#)]. The first four numbers show the location of the goals, forbidden and wall
    squares respectively. 
    The remaining items in the input, determine the output format. The fourth item is either character 
    “p” or “q”, and if it’s “q”, there will be an additional number at the end. 
    Item “p” refers to printing the optimal policy (Π*), and “q” refers to the optimal Q-values (Q*).

<pre>Input and Board Example:
    Input starts with: 15 12 8 6

    Board:
    [13, ][14, ][15,G][16, ]
    [9, ][10, ][11, ][12,G]
    [5, ][6,W][7, ][8,F]
    [1, ][2,S][3, ][4, ]
</pre>

## output:
    -- If the input contains “p”, the program should print the best action that should be chosen for
    each square or in other words print Π*.
    -- If the input contains “q” following a number n, the program should print the four Q-values
    associated with each of the four possible actions in the state that has index n.
